import React from "react";
import "./Footer.scss";

const Footer = () => {
  return (
    <div className="footer">
      <div className="container">
        <h2 className="footer__text">There will be a FOOTER</h2>
        <a className="footer__text" href="#">Contacts</a>
      </div>
    </div>
  );
}

export  default Footer;