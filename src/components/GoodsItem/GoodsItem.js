import React, {memo, useEffect, useState} from "react";
import "./GoodsItem.scss"
import {ReactComponent as FavStar} from "./fav-star.svg";
import {ReactComponent as Cart} from "./cart.svg";
import {ReactComponent as CartFilled} from "./cart-filled.svg";
import Modal from "../Modal/Modal";
import LocalStorage from "../LocalStorage/LocalStorage";
import PropTypes from "prop-types";

const storage = new LocalStorage();

const GoodsItem = (game) => {
    const {id, code, name, price, imgURL, color} = game.game;
    const [isInFavorite, setInFavorite] = useState(false);
    const [isInCart, setInCart] = useState(false);
    const [isModalVisible, setIsModalVisible] = useState(false);

    useEffect(() => {
        if (storage.favorites && storage.favorites.some(storedId => storedId === id)){
            setInFavorite(true);
        }
        if (storage.addedToCart && storage.addedToCart.some(storedId => storedId === id)){
            setInCart( true);
        }
    }, [id]);

    const closeModal = (event) => {
        if (event.target.dataset.action === "close-modal") {
            setIsModalVisible(false);
        }
    }

    const addToCart = () => {
        storage.add("addedToCart", id);
        setIsModalVisible(false);
        setInCart( true);
    }

    const cartBtnClickHandler = () => {
        if (isInCart) {
            storage.remove("addedToCart", id);
            setInCart( false);
        } else {
            setIsModalVisible(true);
        }
    }

    const favoriteBtnClickHandler = () => {
        if (!isInFavorite) {
            storage.add("favorites", id);
        } else {
            storage.remove("favorites", id);
        }
        setInFavorite(!isInFavorite);
    }

    return (
      <>
          <li className="card-list__item card">
              <button className={`fav-btn${isInFavorite ? " added" : ""}`} onClick={favoriteBtnClickHandler}>
                  <FavStar/>
              </button>
              <a className="card__link" href="#">
                  <div className="card__img-wrapper">
                      <img className="card__img" src={imgURL} alt={name}/>
                  </div>
                  <h3 className="card__title">{name}</h3>
              </a>
              <p className="card__info">
                  <span>Артикул: {code} </span>
                  <span>Цвет коробки: {color}</span>
              </p>
              <p className="card__purchase">
                  {price}₴
                  <button className="cart-btn" onClick={cartBtnClickHandler}>
                      {isInCart ? <CartFilled/> : <Cart/>}
                  </button>
              </p>
              <div className="card__params_wrap">
                  <div className="card__params">
                      <p>item 1</p>
                      <p>item 2</p>
                      <p>item 3</p>
                  </div>
              </div>
          </li>
          {isModalVisible && <Modal
            header="Добавить в корзину?"
            closeButton={true}
            text="Вы действительно хотите добавить товар в корзину?"
            actions={
                <div className="cart-confirm-wrapper">
                    <button className="cart-confirm-btn cart-confirm-btn--accept" onClick={addToCart} data-action="close-modal">ОК</button>
                    <button className="cart-confirm-btn cart-confirm-btn--decline" onClick={closeModal} data-action="close-modal">Отмена</button>
                </div>
            }
            closeHandler={closeModal}
          />}
      </>
    );
}


// class GoodsItem extends PureComponent {
//     state = {
//         isInFavorite: false,
//         isInCart: false,
//         modalVisible: false
//     }
//
//     componentDidMount() {
//         const {game: {id}} = this.props;
//         if (storage.favorites && storage.favorites.some(storedId => storedId === id)) this.setState({isInFavorite: true});
//         if (storage.addedToCart && storage.addedToCart.some(storedId => storedId === id)) this.setState({isInCart: true});
//     }
//
//     closeModal = (event) => {
//         if (event.target.dataset.action === "close-modal") {
//             this.setState({modalVisible: false});
//         }
//     }
//
//     addToCart = () => {
//         storage.add("addedToCart", this.props.game.id);
//         this.setState({modalVisible: false, isInCart: true});
//     }
//
//     cartBtnClickHandler = () => {
//         const {isInCart} = this.state;
//         if (isInCart) {
//             storage.remove("addedToCart", this.props.game.id);
//             this.setState({isInCart: false})
//         } else {
//             this.setState({modalVisible: true})
//         }
//     }
//
//     favoriteBtnClickHandler = () => {
//         const {isInFavorite} = this.state;
//         const {game: {id}} = this.props;
//         if (!isInFavorite) {
//             storage.add("favorites", id);
//         } else {
//             storage.remove("favorites", id);
//         }
//         this.setState({isInFavorite: !this.state.isInFavorite});
//     }
//
//     render() {
//         const {game: {code, name, price, imgURL, color}} = this.props;
//         const {isInFavorite, isInCart, modalVisible} = this.state;
//         return (
//             <>
//                 <li className="honey-list__item card">
//                     <button className={`fav-btn${isInFavorite ? " added" : ""}`} onClick={this.favoriteBtnClickHandler}>
//                         <FavStar/>
//                     </button>
//                     <a className="card__link" href="#">
//                         <div className="card__img-wrapper">
//                             <img className="card__img" src={imgURL} alt={name}/>
//                         </div>
//                         <h3 className="card__title">{name}</h3>
//                     </a>
//                     <p className="card__info">
//                         <span>Артикул: {code} </span>
//                         <span>Цвет: {color}</span>
//                     </p>
//                     <p className="card__purchase">
//                         {price}₴
//                         <button className="cart-btn" onClick={this.cartBtnClickHandler}>
//                             {isInCart ? <CartFilled/> : <Cart/>}
//                         </button>
//                     </p>
//                 </li>
//                 {modalVisible && <Modal
//                     header="Добавить в корзину?"
//                     closeButton={true}
//                     text="Вы действительно хотите добавить товар в корзину?"
//                     actions={
//                         <div className="cart-confirm-wrapper">
//                             <button className="cart-confirm-btn cart-confirm-btn--accept" onClick={this.addToCart} data-action="close-modal">ОК</button>
//                             <button className="cart-confirm-btn cart-confirm-btn--decline" onClick={this.closeModal} data-action="close-modal">Отмена</button>
//                         </div>
//                     }
//                     closeHandler={this.closeModal}
//                 />}
//             </>
//         );
//     }
// }

GoodsItem.propTypes = {
    game: PropTypes.object.isRequired
}

export default memo(GoodsItem);