import React, {memo, useCallback, useEffect, useState} from "react";
import GoodsItem from "../GoodsItem/GoodsItem";
import PropTypes from "prop-types";
import "./GoodsList.scss";
import axios from "axios";
import Loader from "../Loader/Loader";

const GoodsList = () => {
    const [gamesData, setGamesData] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    const getProducts = useCallback(async () => {
        const {data} = await axios('/games.json');
        setGamesData(data);
        setIsLoading(false);
    }, []);

    useEffect(() => {
        getProducts();
    }, [getProducts]);

    if(isLoading) return <Loader height="800px" />;

    const gamesCards = gamesData.map( g => <GoodsItem game={g} key={g.id} />)

    return(
      <div>
          <h1 className="title">Online BoardGames Shop</h1>
          <ul className="game-list">
              {gamesCards}
          </ul>
      </div>
    )
}

GoodsList.propTypes = {
    gamesCards: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        imgURL: PropTypes.string.isRequired,
        code: PropTypes.number.isRequired,
        color: PropTypes.string.isRequired,
        type: PropTypes.string
    })).isRequired
}

export default memo(GoodsList);