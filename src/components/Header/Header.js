import React from "react";
import "./Header.scss";
// import LogoIcon from "../../theme/icons/icon-violet.png";
import LogoIcon from "../../theme/icons/icon.png";

const Header = () => {
  return(
    <header className="header">
      <div className="container">
        <div className="header__content">
          <a href="/" className="header__logo">
            <div className="header__logo_img-wrapper">
              <img className="header__logo_img" src={LogoIcon} alt="Icon"/>
            </div>
            <div className="header__logo_title">
              <h1 className="header__logo_title-main">Samodelkin</h1>
              <h2 className="header__logo_title-subtitle">handmade boardgames</h2>
            </div>
          </a>
          <div className="header__order">
            <span className="header__order_text">Готовы сделать заказ или остались вопросы - звоните</span>
            <a className="header__order_tel" href="tel:0931814340">: (093) 181-43-40</a>
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;