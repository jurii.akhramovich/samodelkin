import React from "react";
import GoodsList from "../GoodsList/GoodsList";
import SideBar from "../SideBar/SideBar";
import "./Main.scss";

const Main = () => {
  return (
    <main className="container">
      <div className="main__content">
        <SideBar />
        <GoodsList/>
      </div>
    </main>
  )
}

export default Main;