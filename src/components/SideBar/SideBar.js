import React, {memo} from "react";
import "./SideBar.scss";

const SideBar = () => {
  return (
    <aside>
      <nav className="menu">
        <h3 className="menu__title">Категории</h3>
        <ul className="menu__list">
          <li className="menu__item">
            <a className="menu__item_categories" href="">Все</a>
          </li>
          <li className="menu__item">
            <a className="menu__item_categories" href="#">Воинственные</a>
          </li>
          <li className="menu__item">
            <a className="menu__item_categories" href="#">Для всей семьи</a>
          </li>
          <li className="menu__item">
            <a className="menu__item_categories" href="#">Для больших компаний (5+ игроков)</a>
          </li>
          <li className="menu__item">
            <a className="menu__item_categories" href="#">Дуэльные</a>
          </li>
          <li className="menu__item">
            <a className="menu__item_categories" href="#">Корпоративные</a>
          </li>
          <li className="menu__item">
            <a className="menu__item_categories" href="#">Воинственные</a>
          </li>
          <li className="menu__item">
            <a className="menu__item_categories" href="#">Мои Фавориты</a>
          </li>
        </ul>
      </nav>
    </aside>
  )
}

export default memo(SideBar);